#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include "linked_list.h"
#include "high_functions.h"

static void print_int_in_row(int value) {
    printf("%d ", value);
}
static void print_int_in_column(int value) {
    printf("%d\n", value);
}
static int square(int value) {
    int max = 46340;
    if (value >= max || value <= -max) {
        printf("Возможное переполнение в square(%d), возвращаем -1\n", value);
        return -1;
    }
    return value * value;

}
static int cube(int value) {
    int max = 1290;
    if (value >= max || value <= -max) {
        printf("Возможное переполнение в cube(%d), возвращаем -1\n", value);
        return -1;
    }
    return value * value * value;

}
static int sum(int value, int acc) {
    return value + acc;
}
static int max(int value, int max) {
    return value > max ? value : max;
}

static int min(int value, int min) {
    return value < min ? value : min;
}
static int absolute(int value) {
    return value > 0 ? value : -value;
}

static int twice(int value) {
    return value * 2;
}



static void check_low_functions(list_node **list) {
    int value;
    size_t index = 2;

    puts("Тест функционала 4 лабораторной работы");
    printf("Сумма всех элементов списка равна: %ld\n", list_sum(*list));
    errno = 0;
    value = list_get(*list, index);
    if (errno) {
        perror("Ошибка в list_get: ");
    } else {
        printf("Значение элемента по индексу %lu равен %d\n", index, value);
    }
}

static void check_high_functions(list_node **list) {
    list_node *new_start = NULL;
    list_node **new_list;

    puts("\n Тест функции foreach");

    foreach(*list, print_int_in_row);
    puts("");
    foreach(*list, print_int_in_column);

    puts("\n Тест функции map");

    new_start = map(*list, square);
    foreach(new_start, print_int_in_row);
    puts("");
    list_free(new_start);

    new_start = map(*list, cube);

    foreach(new_start, print_int_in_row);
    puts("");

    puts("\n Тест функции foldl");
    printf("Сумма всех элементов списка равна: %d\n", foldl(*list, 0, sum));
    printf("Максимальный элемент: %d\n", foldl(*list, INT_MIN, max));
    printf("Минимальный элемент: %d\n", foldl(*list, INT_MAX, min));

    puts("\n Тест функции map_mut");
    new_list = list;
    map_mut(new_list, absolute);
    foreach(*new_list, print_int_in_row);
    puts("");


    puts("\n Тест функции iterate");
    new_start = iterate(1, 10, twice);
    foreach(new_start, print_int_in_row); puts("");
    list_free(new_start);
}
static void check_file_functions(list_node **list) {
    list_node *new_iter = NULL;
    list_node **new_list = &new_iter;

    puts("\n Тест save и load");
    if (!save(*list, "list.txt")) {
        perror("Ошибка в save:");
        return;
    }

    if (!load(new_list, "list.txt")) {
        perror("Ошибка в load:");
        list_free(*new_list);
        return;
    }
    puts("Значения, которое сохраняется в файл:");
    foreach(*list, print_int_in_row);
    puts("");
    puts("Значения, которое достается из файла:");
    foreach(*new_list, print_int_in_row);
    puts("");

    list_free(*new_list);

    puts("\n Тест serialize и deserialize");
    if (!serialize(*list, "list.bin")) {
        perror("Ошибка в serialize");
        return;
    }

    if (!deserialize(new_list, "list.bin")) {
        perror("Ошибка в deserialize");
        list_free(*new_list);
        return;
    }
    puts("Значения, которое сериализуются в файл:");
    foreach(*list, print_int_in_row);
    puts("");
    puts("Значения, которое десериализуются из файла:");
    foreach(*new_list, print_int_in_row);
    puts("");

    list_free(*new_list);
}




int main( void ) {
    int value;
    list_node *iter = NULL;
    list_node **list = &iter;

    while(scanf("%d", &value) != EOF){
        list_add_front(list, value);
    }

    check_low_functions(list);
    check_high_functions(list);
    check_file_functions(list);

    list_free(*list);
    return 0;


}
