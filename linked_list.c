

#include <malloc.h>
#include <errno.h>

typedef struct list_node {
    int value;
    struct list_node *next;
} list_node;


list_node *list_create(int number) {
    list_node *node = malloc(sizeof(list_node));
    node->value = number;
    node->next = NULL;
    return node;
}

list_node *list_add_front(list_node **list, int number) {
    list_node *node = list_create(number);
    node->next = *list;
    *list = node;

    return node;

}

list_node *list_add_back(list_node **list, int number) {
    list_node *node = list_create(number);

    if (*list == NULL) {
        *list = node;
    } else {
        list_node *iteration_node = *list;
        while (iteration_node->next != NULL) {
            iteration_node = iteration_node->next;
        }
        iteration_node->next = node;
    }

    return node;

}

list_node *list_node_at(list_node *list, size_t index) {
    list_node *iteration_node = list;
    if (iteration_node == NULL) {
        errno = EFAULT; //Неверный адресс
        return NULL;
    }

    for (size_t i = 0; i < index; i++) {
        if (iteration_node->next == NULL) {
            errno = EINVAL; // Неверный аргумент
            return NULL;
        }
        iteration_node = iteration_node->next;
    }

    return iteration_node;
}


int list_get(list_node *list, size_t index) {
    list_node *search_node = list_node_at(list, index);
    if (search_node == NULL) return -1;
    return search_node->value;
}


void list_free(list_node *list) {
    list_node *iteration_node = list;
    while(iteration_node != NULL) {
        list_node *prev = iteration_node;
        iteration_node = iteration_node->next;
        free(prev);
    }
}


size_t list_length(list_node *list) {
    list_node *iteration_node;
    size_t len = 0;
    for (iteration_node = list; iteration_node != NULL; iteration_node = iteration_node->next)
        len++;
    return len;
}


long list_sum(list_node *list) {
    list_node *iteration_node;
    long sum = 0;
    for (iteration_node = list; iteration_node != NULL; iteration_node = iteration_node->next)
        sum += (long) iteration_node->value;
    return sum;
}

list_node *list_add_after(list_node *node, int value) {
    list_node* new_node = list_create(value);
    if (node != NULL) {
        new_node->next = node->next;
        node->next = new_node;
    }
    return new_node;
}
