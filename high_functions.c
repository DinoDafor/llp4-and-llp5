
#include <bits/types/FILE.h>
#include <errno.h>
#include <stdio.h>
#include "linked_list.h"

void foreach(list_node *list, void func(int)) {
    list_node *iteration_node;
    for (iteration_node = list; iteration_node != NULL; iteration_node = iteration_node->next)
        func(iteration_node->value);
}

list_node* map(list_node *list, int func(int)) {
    list_node *iteration_node, *new_iter = NULL, *start = NULL;

    for (iteration_node = list; iteration_node != NULL; iteration_node = iteration_node->next) {

        new_iter = list_add_after(new_iter, func(iteration_node->value));
        if (start == NULL) start = new_iter;
    }

    return start;
}
list_node *map_mut(list_node **list, int func(int)) {
    list_node *iteration_node;
    for (iteration_node = *list; iteration_node != NULL; iteration_node = iteration_node->next)
        iteration_node->value = func(iteration_node->value);

    return *list;
}
int foldl(list_node *list, int acc, int func(int, int)) {
    list_node *iteration_node;
    for (iteration_node = list; iteration_node != NULL; iteration_node = iteration_node->next)
        acc = func(iteration_node->value, acc);

    return acc;
}
list_node *iterate(int value, size_t length, int func(int)) {
    list_node *start, *iteration_node;

    start = list_create(value);
    iteration_node = start;

    for (size_t i = 1; i < length; i++) {
        value = func(value);
        iteration_node = list_add_after(iteration_node, value);
    }


    return start;
}

int save(list_node *list, const char *filename) {
    list_node *iteration_node;
    FILE *file;
    errno = 0;
    file = fopen(filename, "w");
    if (errno) return 0;

    for (iteration_node = list; iteration_node != NULL; iteration_node = iteration_node->next) {
        fprintf(file, "%d ", iteration_node->value);
        if (errno || ferror(file)) {
            fclose(file);
            return 0;
        }
    }

    fclose(file);
    if (errno) return 0;
    return 1;
}
int load(list_node **list, const char *filename) {
    list_node *iteration_node = NULL, *start = NULL;
    int value;
    FILE *file;
    errno = 0;
    file = fopen(filename, "r");
    if (errno) return 0;

    while (1) {
        fscanf(file, "%d", &value);
        if (feof(file)) break;

        if (errno || ferror(file)) {
            fclose(file);
            return 0;
        }

        iteration_node = list_add_after(iteration_node, value);
        if (start == NULL) start = iteration_node;
    }

    *list = start;
    fclose(file);
    if (errno) return 0;
    return 1;
}
int serialize(list_node *list, const char *filename) {
    list_node *iteration_node;
    FILE *file;
    errno = 0;
    file = fopen(filename, "wb");
    if (errno) return 0;

    for (iteration_node = list; iteration_node != NULL; iteration_node = iteration_node->next) {
        fwrite(&iteration_node->value, sizeof(int), 1, file);
        if (errno || ferror(file)) {
            fclose(file);
            return 0;
        }
    }

    fclose(file);
    if (errno) return 0;
    return 1;
}

int deserialize(list_node **list, const char *filename) {
    list_node *iteration_node = NULL, *start = NULL;
    int value;
    FILE *file;
    errno = 0;
    file = fopen(filename, "r");
    if (errno) return 0;

    while (1) {
        fread(&value, sizeof(int), 1, file);
        if (feof(file)) break;

        if (errno || ferror(file)) {
            fclose(file);
            return 0;
        }

        iteration_node = list_add_after(iteration_node, value);
        if (start == NULL) start = iteration_node;
    }

    *list = start;
    fclose(file);
    if (errno) return 0;
    return 1;
}







