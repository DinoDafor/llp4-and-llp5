
#ifndef LLP5_HIGH_FUNCTIONS_H
#define LLP5_HIGH_FUNCTIONS_H

void foreach(list_node *list, void consumer(int));
list_node* map(list_node *list, int operator(int));
list_node *map_mut(list_node **list, int operator(int));
int foldl(list_node *list, int acc, int operator(int, int));
list_node *iterate(int value, size_t length, int operator(int));
int save(list_node *list, const char *filename);
int load(list_node **list, const char *filename);
int serialize(list_node *list, const char *filename);
int deserialize(list_node **list, const char *filename);

#endif //LLP5_HIGH_FUNCTIONS_H
