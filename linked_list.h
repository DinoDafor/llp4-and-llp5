

#ifndef LLP4_LINKED_LIST_H
#define LLP4_LINKED_LIST_H
#include <stdlib.h>

struct list_node {
    int value;
    struct list_node* next;
};

typedef struct list_node list_node;

list_node *list_create(int number);
list_node *list_add_front(list_node **list, int number);
list_node *list_add_back(list_node **list, int number);
list_node *list_node_at(list_node *list, size_t index);
int list_get(list_node *list, size_t index);
void list_free(list_node *list);
size_t list_length(list_node *list);
long list_sum(list_node *list);
list_node *list_add_after(list_node *node, int value);

#endif //LLP4_LINKED_LIST_H
